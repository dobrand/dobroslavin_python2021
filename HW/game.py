import random

from g_user import Player

percents = {1: 100, 2: 90, 3: 80, 4: 70, 5: 60, 6: 50, 7: 40, 8: 30, 9: 20}

def get_chance(percent: int) -> bool:
    percent /= 10
    chance = random.randint(1, 9)
    return 0 < chance < percent

def valid_power(power: int) -> int:
    return power if get_chance(percents.get(power)) else 0

def punch(player_one: Player, player_two: Player) -> bool:
    power = int(input("{} enter the power,dude - ".format(player_one.username)))
    while not 0 < power < 10:
        power = int(input("wtf mate, try again - ".format(player_one.username)))
    power = valid_power(power)
    if power == 0:
        return False
    else:
        player_two -= power * 10
        return True

if __name__ == '__main__':
    print("okay let's go")
    player_one = Player(input("enter your name, buddy - "))
    player_two = Player(input("now u, bro - "))
    who_is_first = random.randint(1, 2)
    if who_is_first == 2:
        player_two, player_two = player_two, player_one
    flag = True
    while flag:
        if player_two.is_alive():
            if punch(player_one, player_two):
                print("{} yep, u punched".format(player_one.username))
                print(player_two)
            else:
                print("{} sadly, u miss".format(player_one.username))
                print(player_two)
            player_one, player_two = player_two, player_one
        else:
            print("{} damn u go crazy".format(player_one.username))
            flag = False
