def get_array_by_user(count: int) -> list:
    array = []
    for i in range(count):
        array.append(int(input("Введите числа - ")))
    return array

def get_array_sum(lst: sum) -> int:
    sum = 0
    for i in lst:
        sum += i
    return sum

def min_max_of_array(lst: list) -> (int, int):
    if len(lst) > 0:
        min_of_array = lst[0]
        max_of_array = lst[0]
        for elem in lst:
            min_of_array = elem if elem < min_of_array else min_of_array
            max_of_array = elem if elem > max_of_array else max_of_array
    return min_of_array, max_of_array


def get_factorials(lst: list) -> set:
    facts = set()
    for elem in array:
        facts.add(factorial(elem))
    return facts


def factorial(elem):
    fact = 1
    for elem in range(2, elem + 1):
        fact *= elem
    return fact

if __name__ == '__main__':
    array = get_array_by_user(int(input("Введите кол-во элементов списка - ")))
    print(array)
    sum = get_array_sum(array)
    print(sum)
    print(min_max_of_array(array))
    facts = get_factorials(array)
    print(facts)